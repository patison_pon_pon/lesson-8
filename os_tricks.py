import os


if not os.path.isdir('my_folder'):
    os.mkdir('my_folder')
print(os.listdir('.'))
print(os.path.isfile('my_passwords.txt'))

for index, line in enumerate(open('work_with_files/my_passwords.txt')):
    print(index)
    print(line.strip())
    if not os.path.isdir(f"my_folder/pass_dir_{index}"):
        os.mkdir(f"my_folder/pass_dir_{index}")

    with open(f"my_folder/pass_dir_{index}/{line.strip()}", 'w') as my_file:
        my_file.write(line * 50)
