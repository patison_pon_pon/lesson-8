# inputfile = '../my_file.txt'
# – r – открытие документа на чтение;
# – w – возможность записи в файл (все старые данные будут уничтожены), а если его не существует, он то предварительно будет создан;
# – + - одновременные режимы чтения и записи;
# – a – возможность дозаписывать содержимое документа;
# – x – запись в существующий файл (с удалением старого содержимого), в противном случае возникнет ошибка;
# – t – открытие документа в текстовом режиме;
# – b – открытие документа в байтовом режиме.
# print(my_file.read())
inputfile = 'names.txt'
my_file = open(inputfile, 'r')
# print(list(my_file))
for num, line in enumerate(my_file):
    print(f"№{num + 1}: Hello {line.strip()}")


inputfile = 'rockyou.txt'
outputfile = 'my_passwords.txt'
my_inputfile = open(inputfile, 'r', encoding='latin_1')
my_outputfile = open(outputfile, 'w')
# print(list(my_file))
for num, line in enumerate(my_inputfile):
    if 'vasya' in line:
        print(f"Line №: {num}: {line.strip()}")
        my_outputfile.write(line)

my_inputfile.close()
my_outputfile.close()


inputfile = 'rockyou.txt'
outputfile = 'my_passwords.txt'
my_inputfile = open(inputfile, 'r', encoding='latin_1')
my_outputfile = open(outputfile, 'a')
# print(list(my_file))
for num, line in enumerate(my_inputfile):
    if 'petya' in line:
        print(f"Line №: {num}: {line.strip()}")
        my_outputfile.write(line)

my_inputfile.close()
my_outputfile.close()


inputfile = 'rockyou.txt'
outputfile = 'my_passwords.txt'
my_inputfile = open(inputfile, 'r', encoding='latin_1')
my_outputfile = open(outputfile, 'a')
# print(list(my_file))
for num, line in enumerate(my_inputfile):
    if 'kolya' in line:
        print(f"Line №: {num}: {line.strip()}")
        my_outputfile.write(line)

my_inputfile.close()
my_outputfile.close()

with open(outputfile, 'r', encoding='utf-8') as my_outputfile:
    # print(my_outputfile.readlines())
    print(my_outputfile.readline())
    print(my_outputfile.readline())
    print(my_outputfile.readline())
    print(my_outputfile.readline())
    # for line in my_outputfile:
    #     print(line)
